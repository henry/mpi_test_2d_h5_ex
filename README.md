# how to write 2d array of float in hdf5 with C interface in parallel with MPI?

question posée à chatgpt, attention, il doonne mpicc pour compiler, alors qu'il faut h5pcc


Writing a 2D array of floats in an HDF5 file using the C interface in parallel with MPI requires a combination of MPI for parallelism and the HDF5 library for I/O. Here's an example that demonstrates how to do this.

Required Libraries:

    HDF5: Ensure HDF5 is built with parallel support (--enable-parallel during HDF5 installation).
    MPI: You need an MPI environment to run the parallel code.

# Compile and Run:

You will need to link against both the HDF5 and MPI libraries.
## Compile:

```commandline
h5pcc -o hdf5_parallel hdf5_parallel.c -lhdf5 -lhdf5_hl -lm
```

## Run:

```commandline
mpirun -np 4 ./hdf5_parallel
```

This command runs the program on 4 MPI processes, and each process will write a part of the 2D array to the file.
Notes:

    Ensure that you have the HDF5 library compiled with parallel support and linked properly with MPI.
    Modify the dimensions of the array (DIM0 and DIM1) and the number of processes (mpirun -np N) as needed to match your use case.

# Tests

| DIM0*DIM1    | temps | serveur | taille |             |
|--------------|-------| -----|---|-------------|
| 16384*131072 | 10sec | athena-4to | 8.1G | 2^14 * 2^17 |
| 32768*131072 | crash | athena-4to |  | 2^15 * 2^17 |

par contre, ça passe avec 1 seul proc:
```commandline
henry@athena-4to:~/projets/mpi_test_2d_h5_ex$ time mpirun -np 1 build/hdf5_parallel

real	0m31,380s
user	0m9,911s
sys	0m17,528s

```
et aussi avec 2 procs:
```commandline
henry@athena-4to:~/projets/mpi_test_2d_h5_ex$ time mpirun -np 2 build/hdf5_parallel

real	0m25,974s
user	0m10,510s
sys	0m22,970s

```
mais toujours pas avec 4:
```commandline
henry@athena-4to:~/projets/mpi_test_2d_h5_ex$ time mpirun -np 4 build/hdf5_parallel
[athena-4to:3743946] *** Process received signal ***
[athena-4to:3743946] Signal: Segmentation fault (11)
[athena-4to:3743946] Signal code: Address not mapped (1)
[athena-4to:3743946] Failing at address: 0x558014314c08
[athena-4to:3743946] [ 0] /lib/x86_64-linux-gnu/libpthread.so.0(+0x14420)[0x7f00e01cf420]
[athena-4to:3743946] [ 1] /home/henry/LIBRARY_PARA/LIBRARIES/gnu/i8/openmpi-5.0.5/lib/libopen-pal.so.80(+0xbd9f2)[0x7f00dff799f2]
[athena-4to:3743946] [ 2] /home/henry/LIBRARY_PARA/LIBRARIES/gnu/i8/openmpi-5.0.5/lib/libopen-pal.so.80(opal_progress+0x34)[0x7f00dfee0b94]

```