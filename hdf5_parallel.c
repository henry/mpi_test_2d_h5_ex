#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <hdf5.h>

#define FILENAME "parallel_2darray.h5"
#define DATASETNAME "2D_Float_Array"
#define DIM0 8   // Total number of rows
#define DIM1 8   // Total number of columns

int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);

    // Initialize MPI variables
    int mpi_size, mpi_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    // Initialize HDF5 variables
    hid_t file_id, dataset_id, filespace_id, memspace_id;
    hid_t plist_id;
    hsize_t global_dims[2] = {DIM0, DIM1}; // Global dimensions of the array
    hsize_t local_dims[2];                 // Local dimensions (for each process)

    // Calculate the size of each process's local chunk
    local_dims[0] = DIM0 / mpi_size; // Each process writes a part of rows
    local_dims[1] = DIM1;

    // Initialize the local data
    float *local_data = (float *)malloc(local_dims[0] * local_dims[1] * sizeof(float));
    for (long int i = 0; i < local_dims[0]; i++) {
        for (long int j = 0; j < local_dims[1]; j++) {
            local_data[i * local_dims[1] + j] = mpi_rank + 1.0f; // Different data per process
        }
    }

    // Set up file access property list with parallel I/O access
    plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, MPI_COMM_WORLD, MPI_INFO_NULL);

    // Create the file collectively
    file_id = H5Fcreate(FILENAME, H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);

    // Create the dataspace for the dataset (global size)
    filespace_id = H5Screate_simple(2, global_dims, NULL);

    // Create the dataset
    dataset_id = H5Dcreate(file_id, DATASETNAME, H5T_NATIVE_FLOAT, filespace_id,
                           H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    // Define the memory space for local data
    memspace_id = H5Screate_simple(2, local_dims, NULL);

    // Select hyperslab in the file for each process
    hsize_t start[2], count[2];
    start[0] = mpi_rank * local_dims[0]; // Row offset
    start[1] = 0;                        // Column start
    count[0] = local_dims[0];
    count[1] = local_dims[1];
    H5Sselect_hyperslab(filespace_id, H5S_SELECT_SET, start, NULL, count, NULL);

    // Create a dataset transfer property list
    plist_id = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);

    // Write the local data to the dataset
    H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, memspace_id, filespace_id, plist_id, local_data);

    // Close/release resources
    H5Pclose(plist_id);
    H5Sclose(memspace_id);
    H5Sclose(filespace_id);
    H5Dclose(dataset_id);
    H5Fclose(file_id);

    // Free allocated memory
    free(local_data);

    // Finalize MPI
    MPI_Finalize();
    return 0;
}

